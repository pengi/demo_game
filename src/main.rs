mod asset_prefabs;
mod camera;
mod states;

use amethyst::{
    core::TransformBundle,
    input::{InputBundle, StringBindings},
    prelude::*,
    renderer::{
        plugins::{RenderPbr3D, RenderToWindow},
        types::DefaultBackend,
        RenderingBundle,
    },
    utils::application_root_dir,
    LogLevelFilter, LoggerConfig, StdoutLog,
};

use crate::{asset_prefabs::AssetPrefabBundle, states::loading::LoadingState};

fn main() -> amethyst::Result<()> {
    amethyst::start_logger(LoggerConfig {
        stdout: StdoutLog::Colored,
        level_filter: LogLevelFilter::Trace,
        log_file: None,
        allow_env_override: true,
        log_gfx_backend_level: Some(LogLevelFilter::Info),
        log_gfx_rendy_level: Some(LogLevelFilter::Info),
        module_levels: Vec::new(),
    });
    let app_root = application_root_dir()?;
    let display_config_path = app_root.join("config/display.ron");
    let assets_dir = app_root.join("resources/");
    let game_data = GameDataBuilder::default()
        .with_bundle(TransformBundle::new())?
        .with_bundle(AssetPrefabBundle::new())?
        .with_bundle(InputBundle::<StringBindings>::new())?
        .with_bundle(
            RenderingBundle::<DefaultBackend>::new()
                .with_plugin(
                    RenderToWindow::from_config_path(display_config_path)?
                        .with_clear([0.0, 0.0, 0.0, 1.0]),
                )
                .with_plugin(RenderPbr3D::default()),
        )?;

    let mut game = Application::new(assets_dir, LoadingState::default(), game_data)?;
    game.run();
    Ok(())
}
