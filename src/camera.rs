use amethyst::{
    ecs::prelude::World,
    prelude::*,
    core::transform::Transform,
    renderer::camera::Camera,
    window::ScreenDimensions
};

pub fn initialize_camera(world: &mut World) {
    let (width, height) = {
        let dim = world.read_resource::<ScreenDimensions>();
        (dim.width(), dim.height())
    };
    
    let mut transform = Transform::default();
    transform.set_translation_xyz(0.0, -10.0, 18.0);
    transform.prepend_rotation_x_axis(std::f32::consts::PI * 0.125);

    world
        .create_entity()
        .with(transform)
        .with(Camera::standard_3d(width, height))
        .build();
}