use amethyst::{
    animation::{AnimationBundle, VertexSkinningBundle},
    assets::{
        AssetPrefab, AssetStorage, Handle, Prefab, PrefabData, PrefabLoader,
        PrefabLoaderSystemDesc, ProgressCounter, RonFormat,
    },
    core::{transform::Transform, Named, SystemBundle, SystemDesc},
    derive::{PrefabData, SystemDesc},
    ecs::{prelude::*, Component, DenseVecStorage, Entity, World},
    gltf::{GltfSceneAsset, GltfSceneFormat, GltfSceneLoaderSystemDesc},
    utils::application_root_dir,
    Error,
};
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::fs::read_dir;

pub struct AssetPrefabBundle {}

impl AssetPrefabBundle {
    pub fn new() -> AssetPrefabBundle {
        AssetPrefabBundle {}
    }
}

impl<'a, 'b> SystemBundle<'a, 'b> for AssetPrefabBundle {
    fn build(
        self,
        world: &mut World,
        builder: &mut DispatcherBuilder<'a, 'b>,
    ) -> Result<(), Error> {
        builder.add(
            PrefabLoaderSystemDesc::<AssetPrefabData>::default().build(world),
            "asset_prefab_loader",
            &[],
        );
        builder.add(
            GltfSceneLoaderSystemDesc::default().build(world),
            "gltf_loader",
            &["asset_prefab_loader"],
        );
        builder.add(AssetPrefabLoaderSystem, "", &[]);
        AnimationBundle::<usize, Transform>::new("animation_control", "sampler_interpolation")
            .with_dep(&["gltf_loader"])
            .build(world, builder)?;
        VertexSkinningBundle::new()
            .with_dep(&[
                "transform_system",
                "animation_control",
                "sampler_interpolation",
            ])
            .build(world, builder)?;
        Ok(())
    }
}

#[derive(Debug)]
pub struct AssetPrefabTag(String);

impl AssetPrefabTag {
    pub fn new(tag: &str) -> AssetPrefabTag {
        AssetPrefabTag(tag.to_string())
    }
}

impl Component for AssetPrefabTag {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Default, Deserialize, Serialize, PrefabData)]
#[serde(default)]
#[serde(deny_unknown_fields)]
struct AssetPrefabData {
    pub name: Option<Named>,
    pub gltf: Option<AssetPrefab<GltfSceneAsset, GltfSceneFormat>>,
}

#[derive(Default, Debug)]
struct AssetPrefabs {
    prefabs: HashMap<String, Handle<Prefab<AssetPrefabData>>>,
}

impl AssetPrefabs {
    pub fn insert(&mut self, asset_type: String, prefab_handle: Handle<Prefab<AssetPrefabData>>) {
        self.prefabs.insert(asset_type, prefab_handle);
    }
}

pub fn initialize_asset_prefabs(world: &mut World) -> ProgressCounter {
    let mut progress = ProgressCounter::new();
    let prefab_list = {
        /* TODO: Properly traverse prefab dir */
        let prefab_dir_path = application_root_dir()
            .unwrap()
            .join("resources/assets/prefabs")
            .into_os_string()
            .into_string()
            .unwrap();
        read_dir(prefab_dir_path)
            .unwrap()
            .map(|direntry| {
                let filepath = direntry.unwrap().path();
                let filename = filepath.file_name().unwrap().to_str().unwrap();
                format!("assets/prefabs/{}", filename)
            })
            .map(|relpath| {
                world.exec(|loader: PrefabLoader<'_, AssetPrefabData>| {
                    println!("Loading asset {:?}", relpath);
                    loader.load(relpath, RonFormat, &mut progress)
                })
            })
    };
    let mut asset_prefabs = AssetPrefabs::default();
    for (count, prefab) in prefab_list.enumerate() {
        asset_prefabs.insert("temp_asset_".to_string() + &count.to_string(), prefab);
    }
    world.insert(asset_prefabs);
    progress
}

pub fn update_asset_prefabs(world: &mut World) {
    let fetched_prefabs = world.try_fetch_mut::<AssetPrefabs>();
    let fetched_storage = world.try_fetch_mut::<AssetStorage<Prefab<AssetPrefabData>>>();
    println!("update_asset_prefabs");
    if let (Some(mut prefabs), Some(mut storage)) = (fetched_prefabs, fetched_storage) {
        let mut new_prefabs = HashMap::new();
        for (tmpname, handle) in &prefabs.prefabs {
            let prefab_data = storage.get_mut(handle).unwrap().entity(0).unwrap();
            let name = prefab_data
                .data()
                .unwrap()
                .name
                .as_ref()
                .unwrap()
                .name
                .to_string();
            println!("Adding {:?} = {:?}", tmpname, name);
            new_prefabs.insert(name, handle.clone());
        }
        prefabs.prefabs = new_prefabs;
    }
    println!("Loading done");
}

#[derive(SystemDesc)]
struct AssetPrefabLoaderSystem;

impl<'a> System<'a> for AssetPrefabLoaderSystem {
    type SystemData = (
        ReadStorage<'a, AssetPrefabTag>,
        WriteStorage<'a, Handle<Prefab<AssetPrefabData>>>,
        ReadExpect<'a, AssetPrefabs>,
        Entities<'a>,
    );

    fn run(&mut self, (tags, mut prefabs, prefab_templates, entities): Self::SystemData) {
        let mut to_add: Vec<(Entity, &String)> = Vec::new();
        for (AssetPrefabTag(tag), _, entity) in (&tags, !&prefabs, &entities).join() {
            to_add.push((entity.clone(), tag));
        }
        for (entity, tag) in to_add {
            println!("Loading {:?} as {:?}", entity, tag);
            prefabs
                .insert(entity, prefab_templates.prefabs.get(tag).unwrap().clone())
                .unwrap();
        }
    }
}
