use crate::{
    asset_prefabs::{initialize_asset_prefabs, update_asset_prefabs},
    states::arena::ArenaState,
};

use amethyst::{assets::ProgressCounter, prelude::*};

pub struct LoadingState {
    prefab_loading_progress: Option<ProgressCounter>,
}

impl Default for LoadingState {
    fn default() -> Self {
        LoadingState {
            prefab_loading_progress: None,
        }
    }
}

impl SimpleState for LoadingState {
    fn on_start(&mut self, mut data: StateData<'_, GameData<'_, '_>>) {
        self.prefab_loading_progress = Some(initialize_asset_prefabs(&mut data.world));
    }

    fn update(&mut self, data: &mut StateData<'_, GameData<'_, '_>>) -> SimpleTrans {
        data.data.update(&data.world);
        if let Some(ref counter) = self.prefab_loading_progress.as_ref() {
            if counter.is_complete() {
                self.prefab_loading_progress = None;
                update_asset_prefabs(&mut data.world);
                println!("Loaded, next state");
                return Trans::Switch(Box::new(ArenaState::default()));
            }
        }

        Trans::None
    }
}
