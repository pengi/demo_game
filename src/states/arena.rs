use amethyst::{
    assets::AssetLoaderSystemData,
    core::math::Vector3,
    core::Transform,
    ecs::prelude::World,
    prelude::*,
    renderer::{
        light::{Light, PointLight},
        mtl::{Material, MaterialDefaults},
        palette::{LinSrgba, Srgb},
        rendy::{
            mesh::{Normal, Position, Tangent, TexCoord},
            texture::palette::load_from_linear_rgba,
        },
        shape::Shape,
        Mesh, Texture,
    },
};

use crate::{asset_prefabs::AssetPrefabTag, camera::initialize_camera};

#[derive(Default)]
pub struct ArenaState;

impl SimpleState for ArenaState {
    fn on_start(&mut self, data: StateData<'_, GameData<'_, '_>>) {
        let world = data.world;
        println!("Starting ArenaState");
        initialize_camera(world);
        initialize_floor(world);
        initialize_lights(world);
        initialize_player(world);
    }
}

fn initialize_lights(world: &mut World) {
    let light: Light = PointLight {
        intensity: 6.0,
        color: Srgb::new(1.0, 1.0, 1.0),
        ..PointLight::default()
    }
    .into();
    let mut pos = Transform::default();
    pos.set_translation_xyz(-10.0, -5.0, 10.0);

    world.create_entity().with(light).with(pos).build();
}

fn initialize_floor(world: &mut World) {
    let mat_defaults = world.read_resource::<MaterialDefaults>().0.clone();
    let mesh = world.exec(|loader: AssetLoaderSystemData<'_, Mesh>| {
        loader.load_from_data(
            Shape::Plane(None)
                .generate::<(Vec<Position>, Vec<Normal>, Vec<Tangent>, Vec<TexCoord>)>(None)
                .into(),
            (),
        )
    });
    let albedo = [
        world.exec(|loader: AssetLoaderSystemData<'_, Texture>| {
            loader.load_from_data(
                load_from_linear_rgba(LinSrgba::new(1.0, 1.0, 1.0, 1.0)).into(),
                (),
            )
        }),
        world.exec(|loader: AssetLoaderSystemData<'_, Texture>| {
            loader.load_from_data(
                load_from_linear_rgba(LinSrgba::new(0.3, 0.3, 0.3, 1.0)).into(),
                (),
            )
        }),
    ];
    for i in 0..12 {
        for j in 0..12 {
            let roughness = 0.5f32;
            let metallic = 0.1f32;
            let mut pos = Transform::default();
            pos.set_translation_xyz(
                2.0f32 * (i as f32 - 5.5f32) as f32,
                2.0f32 * (j as f32 - 5.5f32) as f32,
                0.0,
            );
            let mtl = world.exec(
                |(mtl_loader, tex_loader): (
                    AssetLoaderSystemData<'_, Material>,
                    AssetLoaderSystemData<'_, Texture>,
                )| {
                    let metallic_roughness = tex_loader.load_from_data(
                        load_from_linear_rgba(LinSrgba::new(0.0, roughness, metallic, 0.0)).into(),
                        (),
                    );
                    mtl_loader.load_from_data(
                        Material {
                            albedo: albedo[(i + j) % 2].clone(),
                            metallic_roughness,
                            ..mat_defaults.clone()
                        },
                        (),
                    )
                },
            );
            world
                .create_entity()
                .with(pos)
                .with(mesh.clone())
                .with(mtl)
                .build();
        }
    }
}

fn initialize_player(world: &mut World) {
    let mut pos = Transform::default();
    pos.set_translation_xyz(0.0, 0.0, 3.0);
    let scale = 0.01f32;
    pos.set_scale(Vector3::new(scale, scale, scale));

    world
        .create_entity()
        .with(pos)
        .with(AssetPrefabTag::new("player"))
        .build();
}
